<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Books;
use App\Form\BooksFormType;
use Symfony\Component\HttpFoundation\Request;

class BookController extends AbstractController
{

    /**
     * @Route("/books", name="getBooks")
     */
    public function getAllBooks(EntityManagerInterface $doctrine)
    {
        $repo = $doctrine->getRepository(Books::class);
        $books = $repo->findAll();
        return $this->render("book/book.html.twig", ["books" => $books]);
    }

    /**
     * @Route("/books/create", name="postNewBook")
     */
    public function postNewBook(Request $req, EntityManagerInterface $doctrine)
    {
        $form = $this->createForm(BooksFormType::class);
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {
            $book = $form->getData();
            $user = $this->getUser();
            //dumnp($user)
            $book->setIdUser($user);
            $doctrine->persist($book);
            $doctrine->flush();
            return $this->redirectToRoute("getBooks");
        }
        return $this->render("book/formNewBook.html.twig", ['booksForm'=>$form->createView()]);
    }
}
